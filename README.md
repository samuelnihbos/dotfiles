
# ![banner](assets/dotfiles.png) 


## 📍️**My Setup**

- **OS :** ArcoLinux
- **Terminal :** kitty, urxvt, [St](https://gitlab.com/st-samuel)
- **Shell :** Fish with Starship Prompt
- **Panel :** Xmobar (xmonad), Polybar (i3)
- **WM :** Xmonad, i3-gaps 
- **App launcher :** rofi, [Dmenu-distrotube](https://gitlab.com/dwt1/dmenu-distrotube) 
- **File manager :** pcmanfm
- **Compositor:** picom-jonaburg-git
- **Fonts:** SauceCodePro nerd font, Ubuntu family font, Source Code Pro, Hack, Noto Sans CJK. Overpass, etc

## 🔰️**Installation (manual, haven't created automated script for this)**

>**Fish with starship prompt**

install fish and change the default shell
```bash
sudo pacman -S fish 
```
```bash
sudo chsh $USER -s /bin/fish
```

install starship prompt
```bash
sh -c "$(curl -fsSL https://starship.rs/install.sh)"
```
This will install starship prompt

> **Window manager**

```bash
sudo pacman -S i3-gaps xmonad xmonad-contrib xmonad-utils
```

>**Polybar (You can use any of aur helper. I use yay)**

```bash
yay -S polybar
```

>**rofi and any additional things**

```bash
sudo pacman -S figlet neofetch pcmanfm grub-customizer scrot vim nitrogen
```


```bash
yay -S ttf-dejavu ttf-droid ttf-font-awesome ttf-hack ttf-ms-fonts ttf-nerd-fonts-symbols ttf-roboto ttf-roboto-mono ttf-ubuntu-font-family otf-overpass otf-raleway picom rxvt-unicode rxvt-unicode-terminfo scrot rofi picom-jonaburg-git kitty
```

>**clone and copying files**

```bash
git clone https://gitlab.com/samuelnihbos/dotfiles.git
```

Now, copy or move **.vim .vimrc .Xresources .xmonad and .zshrc** into your home directory. After that, **copy all things** from **.config** folder and paste it into your **~/.config** folder. Then, go to your **Pictures** directory (usually ~/Pictures), and make a new folder called wal. **Go back to the dotfiles directory**, and copy all images inside **Wallpaper**. Paste all the file into wal folder. And you are done. you can now log out and login to your fresh window manager session!

# ![screenshot](assets/contains.png) 
>**Here's the screenshots of my current setup**

i3wm

![screenshot](assets/sc.jpg) 

Xmonad

![screeenshot](assets/xmonad.png)

**You may need to change some of the config that may be broken or doesn't fit to your style. And you also need to download font fron external resource if the font isn't available on the list. Also, if you having trouble discovering the keybindings on xmonad, you can press SUPER+SHIFT+/ to get a list of keybindings**

# Credits
- [DistroTube's Gitlab](https://gitlab.com/dwt1) 
- [Fish Shell](https://github.com/fish-shell/fish-shell) 
- [Terminal Colorschemes from here](http://dotshare.it/dots/8448/)
- [TheRepoClub](https://github.com/The-Repo-Club)
- [Erik Dubois Leftwm Config](https://github.com/erikdubois)
